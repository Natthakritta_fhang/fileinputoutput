/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.ractangle;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Natthakritta
 */
public class Computer implements Serializable{

    private int hand;
    private int player;
    private int win, lose, draw;
    private int status;

    public Computer() {

    }

    private int choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int paoYingChoob(int player) {//win 1 draw 0 lose -1
        this.player = player;
        this.hand = choob();
        if (this.player == this.hand) {
            draw++;
            status = 0;
            return 0;
        } else if (this.player == 0 && this.hand == 1) {
            win++;
            status = 1;
            return 1;
        } else if (this.player == 1 && this.hand == 2) {
            win++;
            status = 1;
            return 1;
        } else if (this.player == 2 && this.hand == 0) {
            win++;
            status = 1;
            return 1;
        }
        lose++;
        status = -1;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayer() {
        return player;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }

}
